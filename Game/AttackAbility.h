#pragma once
#include "Ability.h"
class AttackAbility :
	public Ability
{
public:
	virtual void use_ability(BasePlayer *owner, const std::shared_ptr<BasePlayer>& target) override;
	AttackAbility(int min, int max);
	~AttackAbility();
};

