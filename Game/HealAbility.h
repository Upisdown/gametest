#pragma once
#include "Ability.h"
class HealAbility :
	public Ability
{
public:
	virtual void use_ability(BasePlayer *owner, const std::shared_ptr<BasePlayer>& target) override;
	HealAbility(int min, int max);
	~HealAbility();
};

