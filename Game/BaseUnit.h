#pragma once
class BaseUnit
{
public:
	virtual void Action() = 0;
	BaseUnit();
	~BaseUnit();
};

