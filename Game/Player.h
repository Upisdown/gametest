#pragma once
#include "BasePlayer.h"

class Player :
	public BasePlayer
{
public:
	virtual void action() override;
	Player(int max_health, string name);
	~Player();
};

