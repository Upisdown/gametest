#include "pch.h"
#include "BasePlayer.h"
#include <iostream>

using namespace std;




bool BasePlayer::is_alive()
{
	return !is_dead;
}


std::string BasePlayer::get_name() const
{
	return name;
}

int BasePlayer::get_health() const
{
	return health;
}

void BasePlayer::print_hp() const
{
	cout << name << ": " << health << setw(7) <<  "HP.";
}

void BasePlayer::change_health(int dealta_health) 
{
	health += dealta_health;			//�������� ���� hp �� �������� ��������

	if (health <= 0)					//��������� �� ����� �� ��� �� �����
	{
		health = 0;
		is_dead = true;
	}
	if (health >= max_health)
	{
		health = max_health;
	}

	
}


void BasePlayer::add_ability(shared_ptr<Ability> ability)
{
	if (ability)
	{
		abilities.push_back(ability);
	}
	
}

void BasePlayer::set_target(shared_ptr<BasePlayer> new_target)
{
	target = new_target;
}

BasePlayer::BasePlayer()  
{
}


BasePlayer::BasePlayer(int max_health, string name) :
	max_health(max_health),
	health(max_health),
	name(name),
	is_dead(false)
{
	if (width < name.length())			//������� ����� ������� ��� ��� ����������� ������
		width = name.length();
}

BasePlayer::~BasePlayer()
{
	
}
