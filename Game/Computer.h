#pragma once
#include "BasePlayer.h"
#include <string>
 using namespace std;
class Computer :
	public BasePlayer
{
protected:
	double health_level;
public:
	virtual void action() override;
	Computer(int max_health, string name, double health_level);
	~Computer();
};

