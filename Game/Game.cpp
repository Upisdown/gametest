// Game.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "BasePlayer.h"
#include "Ability.h"
#include "AttackAbility.h"
#include "HealAbility.h"
#include "Computer.h"
#include "Player.h"
#include <cstdlib>
#include <ctime>

using namespace std;

bool get_range(string command, int& out_min, int& out_max)		//������� ��� ��������� ��������� ����� �� ��������� ������
{
	int temp1, temp2;
	while (true)
	{
		cout << command;
		cin >> temp1 >> temp2;
		if (cin && temp1 >= 0 && temp2 >= temp1)				//���� ����� ����������, �� ������� �� � ������� �� �������
		{
			out_min = temp1;
			out_max = temp2;
			return true;
		}
		else 
		{														//����� ������� ����� ����� 
			cin.clear();
			while (cin.get() != '\n');
		}
	}
}

int get_int(string command)										//������� ��� ��������� ����� �� ��������� ������
{	
	int temp;
	while (true)
	{	
		cout << command;
		cin >> temp;
		if (cin && temp > 0)									//���� ����� ����������, �� ���������� ���
		{
			return temp;
		}
		else
		{														//����� ������� ����� ����� 
			cin.clear();
			while (cin.get() != '\n');
		}
	}
}

int BasePlayer::width = 0;					//�������������� ���� ������ ���� ������� ��������� 0

int main()
{
	int max_health,
		comp_health_lvl,
		range_of_normal_damage_min,
		range_of_normal_damage_max,
		range_of_heavy_damage_min,
		range_of_heavy_damage_max;
	string player_name = "",
		   comp_name = "";	

	// ��������� ��������� ��������

	cout << "Input name of player character: ";
	cin >> player_name;
	cout << "Input name of computer character: ";
	cin >> comp_name;
	max_health = get_int("Input amount of health for both players: ");
	comp_health_lvl = get_int("Input health level when chance to heal for comp must be increased: ");
	get_range("Input range of damage by normal attack: ", range_of_normal_damage_min, range_of_normal_damage_max);	
	get_range("Input range of damage by heavy attack: ", range_of_heavy_damage_min, range_of_heavy_damage_max);	
	cout << endl << endl;
	
	//�������� ������� �������
	vector<shared_ptr<BasePlayer>> players;

	//������� ��������� �� ���������� ������� ������ � ����������
	auto comp = make_shared<Computer>(max_health, comp_name, comp_health_lvl);
	auto player = make_shared<Player>(max_health, player_name);

	//������ ���� ��� ������
	comp->set_target(player);
	player->set_target(comp);

	//������� ������ ������
	vector<shared_ptr<Ability>> abilities = { 
		make_shared<AttackAbility>(range_of_normal_damage_min, range_of_normal_damage_max),
		make_shared<AttackAbility>(range_of_heavy_damage_min, range_of_heavy_damage_max),
		make_shared<HealAbility>(range_of_normal_damage_min, range_of_normal_damage_max)
	};

	//��������� ��������� ��������� �����
	std::srand((unsigned)time(NULL));

	//���������� ������������������ �����
	if (rand() % 2)
	{
		players.push_back(comp);
		players.push_back(player);
	}
	else
	{
		players.push_back(player);
		players.push_back(comp);		
	}

	//��������� ������ ��� �������
	for (auto p : players)
	{
		for (auto a : abilities)
		{
			p->add_ability(a);			
		}
	}
		
	cout << setiosflags(ios::left); // ������������ ������ �� ����� �������
	int step_num = 1;
	bool all_alive = true;
	while (all_alive)											// �������� ������� ����
	{		
		for (auto p : players)
		{	
			cout << step_num++ << ". ";							//������� ������� ����
			p->action();										//������������� �����������	
			for (auto p : players)
			{
				p->print_hp();								//������� ���-�� hp ������� ������
				if (!(p->is_alive()))							//��������� �� ������ 
				{
					all_alive = false;					
				}
			}
			cout << endl << "   ===========================================================================" << endl;
			if (!all_alive)
			{
				cout << "     ===== " << p->get_name() << " WIN!!!" << " =====" << endl;		//������� ����������
				break;																			//��������� ���� ����� ������� ���� ���-�� �� ��� �����
			}							
		}			
	}	


	return 0;
}



