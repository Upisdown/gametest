#pragma once

#include "BasePlayer.h"
#include <iomanip>

enum class e_ability_types { attack, heal };       //����������� ����� ������������

class BasePlayer;
class Ability
{

protected:
	
	int min;									//����������� � 
	int max;									//������������ �������� 
	e_ability_types type;						//�����(������)
	int get_value();							//��������� ����� � �������� ���������

public:
	e_ability_types get_type();	
	virtual void use_ability(BasePlayer *owner, 
		const std::shared_ptr<BasePlayer>& target)=0;
	Ability(int min, int max, e_ability_types type);
	Ability();
	~Ability();
};

