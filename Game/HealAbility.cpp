#include "pch.h"
#include "HealAbility.h"
#include <iostream>
#include <iomanip>

using namespace std;

void HealAbility::use_ability(BasePlayer *owner, const std::shared_ptr<BasePlayer>& target)
{
	if (owner)
	{
		int heal_value = get_value();								//�������� �������� ������ �����������
		owner->change_health(heal_value);							//�������� hp ��������� �� �������� ��������
		cout << setw(BasePlayer::width) << owner->get_name()		//������� ���������� � ������������� �����������
			<< setw(13+BasePlayer::width) <<" restore " 
			<< heal_value << setw(10) << "HP.";
	}
}

HealAbility::HealAbility(int min, int max): Ability(min, max, e_ability_types::heal)
{	
}


HealAbility::~HealAbility()
{
}
