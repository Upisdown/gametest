#include "pch.h"
#include "AttackAbility.h"
#include <iostream>
using namespace std;


void AttackAbility::use_ability(BasePlayer *owner, const std::shared_ptr<BasePlayer>& target)
{
	if (target)
	{
		int damage_value = get_value();											//�������� �������� ����� �����������
		target->change_health(-damage_value);									//�������� hp ���� �� �������� ��������
		cout << setw(BasePlayer::width) <<owner->get_name() << " attack "		//������� ���������� � ������������� �����������
			<< setw(BasePlayer::width) 	<< target->get_name() <<  " for " 
			<< damage_value << setw(10) << "HP.";
		
	}

}

AttackAbility::AttackAbility(int min, int max): Ability(min, max, e_ability_types::attack)
{
}


AttackAbility::~AttackAbility()
{
}
