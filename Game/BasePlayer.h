#pragma once

#include "Ability.h"
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

class Ability;

class BasePlayer
{
protected:
	bool is_dead;
	int max_health;
	int health;	
	string name;
	weak_ptr<BasePlayer> target;
	vector<shared_ptr<Ability>> abilities;
		
public:
	bool is_alive();
	string get_name() const;
	int get_health() const;	
	void print_hp() const;				
	void change_health(int dealta_health);
	void add_ability(shared_ptr<Ability> ability);	
	void set_target(shared_ptr<BasePlayer> new_target);
	virtual void action() = 0;
	BasePlayer(int max_health, string name);
	BasePlayer();
	~BasePlayer();

	static int width;				//���� ���������� �� ������������ ����� �����
	friend class Ability;
};

